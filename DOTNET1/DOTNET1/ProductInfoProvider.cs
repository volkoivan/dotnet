﻿using System;
namespace DOTNET1 {
	public class ProductInfoProvider : IInfo<IProduct> {
		/// <summary>
		/// Выводит в консоль информацию о продукте
		/// </summary>
		/// <param name="obj">Продукт.</param>
		public void PrintInfo(IProduct obj) {
			Console.WriteLine("This product name is {0}", obj.GetName());
		}
	}
}

