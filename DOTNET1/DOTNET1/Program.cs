﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

namespace DOTNET1 {

	class MainClass {
		//сортировка продуктов по весу
		private static int SortByWeight(IProduct a, IProduct b) {
			return a.GetWeight().CompareTo(b.GetWeight());
		}

		//сортировка продуктов по имени (в алфавитном порядке)
		private static int SortByName(IProduct a, IProduct b) {
			return a.GetName().CompareTo(b.GetName());
		}

		public static void Main(string[] args) {
			Console.WriteLine("Welcome to chocolate factory!");
			var gift = Gift.FromFile("/Users/volkoivan/Developer/DOTNET/DOTNET1/DOTNET1/Input/InputCount.txt");

			//Json Serialization
			var jsonSerializator = new JsonSerializer<Gift>();
			TestSerializer(jsonSerializator, "gift.json", gift);

			//Binary Serialization
			var binarySerializator = new BinarySerializator<Gift>();
			TestSerializer(binarySerializator, "gift.bin", gift);

			//Xml Serialization
			var xmlSerializator = new CustomXmlSerializer<Gift>();
			TestSerializer(xmlSerializator, "gift.xml", gift);

            Console.Read();
		}

		private static void TestSerializer(ISerializer<Gift> serializer, string path, Gift gift) {
			serializer.Serialize(gift, path);
			var gift2 = serializer.Unserialize(path);
			gift2.PrintGift();
			Console.WriteLine();
		}
	}
}
