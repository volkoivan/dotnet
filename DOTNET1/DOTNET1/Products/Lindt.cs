using System;
using System.Collections.Generic;

namespace DOTNET1 {
	[Serializable]
	public class Lindt : ChocolateBar {
		static Random random = new Random();
		public Lindt() {
			this.name = "Lindt";
			this.weight = random.Next(125, 135);
		}
	}
}
