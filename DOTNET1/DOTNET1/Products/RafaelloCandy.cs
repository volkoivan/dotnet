using System;
using System.Collections.Generic;

namespace DOTNET1 {
	[Serializable]
	public class RafaelloCandy : Candy, IRafaelloCandy {

		static Random random = new Random();
		public RafaelloCandy() {
			this.name = "Rafaello";
			this.weight = random.Next(35, 45);
		}
	}
}
