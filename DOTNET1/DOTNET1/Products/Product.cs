using System;
using System.Collections.Generic;
using System.Xml;

namespace DOTNET1 {
	[Serializable]
	public class Product: IProduct  {
		/// <summary>
		/// Название продукта
		/// </summary>
		[System.Xml.Serialization.XmlAttribute]
		public string name;
		/// <summary>
		/// Вес продукта в граммах
		/// </summary>
		[System.Xml.Serialization.XmlAttribute]
		public int weight;
		public Product() {
		}

		public Product(string name, int weight): base() {
			this.name = name;
			this.weight = weight;
		}

		string IProduct.GetName() {
			return name;
		}

		int IProduct.GetWeight() {
			return weight;
		}
	}
}
