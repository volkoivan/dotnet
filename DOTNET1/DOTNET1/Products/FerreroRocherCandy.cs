using System;
using System.Collections.Generic;

namespace DOTNET1 {
	[Serializable]
	public class FerreroRocherCandy : Candy, IFerreroRocherCandy {

		static Random random = new Random();
		public FerreroRocherCandy() {
			this.name = "Ferrero Rocher";
			this.weight = random.Next(45, 55);
		}
	}
}
