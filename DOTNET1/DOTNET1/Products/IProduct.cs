﻿using System;
namespace DOTNET1 {
	public interface IProduct {
		/// <summary>
		/// Возвращает имя продукта
		/// </summary>
		/// <returns>Имя.</returns>
		string GetName();
		int GetWeight();
	}
}

