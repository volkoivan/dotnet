using System;
using System.Collections.Generic;

namespace DOTNET1 {
	[Serializable]
	public class Milka : ChocolateBar {
		static Random random = new Random();
		public Milka() {
			this.name = "Milka";
			this.weight = random.Next(145, 155);
		}

	}
}
