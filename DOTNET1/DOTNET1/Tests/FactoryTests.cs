﻿using System;
using NUnit.Framework;
namespace DOTNET1 {
	/// <summary>
	/// Factory tests.
	/// </summary>
	public class FactoryTests {
		/// <summary>
		/// Tests the black factory.
		/// </summary>
		[Test]
		public void TestBlackFactory() {
			var factory = new BlackChocolateFactory();
			Assert.NotNull(factory);
			var candy = factory.CreateCandy();
			var bar = factory.CreateChocolateBar();
			Assert.AreEqual(candy.name, "Ferrero Rocher");
			Assert.AreEqual(bar.name, "Lindt");
		}
		/// <summary>
		/// Tests the white factory.
		/// </summary>
		[Test]
		public void TestWhiteFactory() {
			var factory = new WhiteChocolateFactory();
			Assert.NotNull(factory);
			var candy = factory.CreateCandy();
			var bar = factory.CreateChocolateBar();
			Assert.AreEqual(candy.name, "Rafaello");
			Assert.AreEqual(bar.name, "Milka");
		}
	}
}
