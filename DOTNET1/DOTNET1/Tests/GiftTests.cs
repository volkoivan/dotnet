﻿using System;
using NUnit.Framework;

namespace DOTNET1 {
	/// <summary>
	/// Gift tests.
	/// </summary>
	[TestFixture]
	public class GiftTests {
		const string path = "/Users/volkoivan/Developer/DOTNET/DOTNET1/DOTNET1/Input/InputCount.txt";
		const string badFormatPath = "/Users/volkoivan/Developer/DOTNET/DOTNET1/DOTNET1/Input/BadInputCount.txt";
		/// <summary>
		/// Gift creation test.
		/// </summary>
		[Test]
		public void CreateGiftTest() {
			Gift gift = new Gift(5, 4, "black");
			Assert.AreEqual(9, gift.products.Count);
			CollectionAssert.AllItemsAreNotNull(gift.products);
		}

		/// <summary>
		/// Gift clear test.
		/// </summary>
		[Test]
		public void ClearGiftTest() {
			Gift gift = new Gift(5, 4, "black");
			gift.Clear();
			CollectionAssert.IsEmpty(gift.products);
		}

		/// <summary>
		/// Gift from file test.
		/// </summary>
		[Test]
		public void FromFileTest() {
			var gift = Gift.FromFile(path);
			CollectionAssert.AllItemsAreNotNull(gift.products);
			Assert.NotNull(gift);
		}

		/// <summary>
		/// File format test
		/// </summary>
		[Test]
		public void FileFormatTest() {
			Assert.Throws<FileFormatException>(() => {
				var gift = Gift.FromFile(badFormatPath);
			});
		}

		/// <summary>
		/// Gift add test.
		/// </summary>
		[Test]
		public void AddTest() {
			Gift gift = new Gift(5, 4, "black");
			var ferrero = new FerreroRocherCandy();
			gift.Add(ferrero);
			CollectionAssert.Contains(gift.products, ferrero);
		}

		/// <summary>
		/// Gift remove test
		/// </summary>
		[Test]
		public void RemoveTest() {
			Gift gift = new Gift(5, 4, "black");
			var product = gift.products[0];
			Assert.DoesNotThrow(() => gift.Remove(product));
			CollectionAssert.DoesNotContain(gift.products, product);
		}

		/// <summary>
		/// Gift sort test.
		/// </summary>
        [Test]
		public void SortTest() {
			Gift gift = new Gift(1, 1, "black");
			gift.Sort((a, b) => String.Compare(a.GetName(), b.GetName(), StringComparison.Ordinal));
			Assert.IsTrue(gift.products[0].name == "Ferrero Rocher");
        }
	}
}
