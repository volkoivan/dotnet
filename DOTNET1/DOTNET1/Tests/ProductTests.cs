﻿using System;
using NUnit.Framework;
namespace DOTNET1 {
	/// <summary>
	/// Product tests.
	/// </summary>
	[TestFixture]
	public class ProductTests {
        [TestCase("Ferrero Rocher", 8,TestName = "CreateFerrero")]
		[TestCase("Rafaello", 4, TestName = "CreateRafaello")]
		public void TestInit(string name, int weight) {
			var product = new Product(name, weight);
			Assert.AreEqual(product.name, name);
			Assert.AreEqual(product.weight, weight);
		}
	}
}
