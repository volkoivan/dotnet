﻿using System;
using System.IO;
using System.Collections.Generic;
using NUnit.Framework;
namespace DOTNET1 {
	/// <summary>
	/// Serializer tests.
	/// </summary>
	[TestFixture]
	public class SerializerTests {
		private Gift gift = new Gift(5, 4, "black");

		private const string pathXml = "/Users/volkoivan/Developer/DOTNET/DOTNET1/DOTNET1/bin/Debug/gift.xml";
		private const string pathJson = "/Users/volkoivan/Developer/DOTNET/DOTNET1/DOTNET1/bin/Debug/gift.json";
		private const string pathBin = "/Users/volkoivan/Developer/DOTNET/DOTNET1/DOTNET1/bin/Debug/gift.bin";

		/// <summary>
		/// Deletes the files after completion.
		/// </summary>
		[OneTimeTearDown]
		public void DeleteFiles() {
			File.Delete(pathXml);
			File.Delete(pathJson);
			File.Delete(pathBin);
		}

		/// <summary>
		/// Tests the xml serializer.
		/// </summary>
		[Test]
		public void TestXml() {
			var serializer = new CustomXmlSerializer<Gift>();
			serializer.Serialize(gift, pathXml);
			var serGift = serializer.Unserialize(pathXml);
			CollectionAssert.AreEqual(gift.products.ToString(), serGift.products.ToString());
		}

		/// <summary>
		/// Tests the json serializer.
		/// </summary>
		[Test]
		public void TestJson() {
			var serializer = new JsonSerializer<Gift>();
			serializer.Serialize(gift, pathJson);
			var serGift = serializer.Unserialize(pathJson);
			CollectionAssert.AreEqual(gift.products.ToString(), serGift.products.ToString());
		}

		/// <summary>
		/// Tests the bin serializer.
		/// </summary>
		[Test]
		public void TestBin() {
			var serializer = new BinarySerializator<Gift>();
			serializer.Serialize(gift, pathBin);
			var serGift = serializer.Unserialize(pathBin);
			CollectionAssert.AreEqual(gift.products.ToString(), serGift.products.ToString());
		}
	}
}
