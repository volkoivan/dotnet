using System;
using System.Collections.Generic;

namespace DOTNET1 {
	/// <summary>
	/// Абстрактный класс фабрики
	/// </summary>
	public abstract class AbstractFactory : IFactory {

		/// <summary>
		/// Создаёт новую абстрактную фабрику
		/// </summary>
		/// <param name="candiesInPack">Количество конфет в подарке.</param>
		/// <param name="chocolateBarsInPack">Количество шоколадок в подарке.</param>
		/// <param name="name">Имя фабрики.</param>
		public AbstractFactory(int candiesInPack, int chocolateBarsInPack, string name) {
			this.candiesInPack = candiesInPack;
			this.chocolateBarsInPack = chocolateBarsInPack;
			this.name = name;
			Console.WriteLine("{0} factory created!", name);
		}

		private string name;
		private int chocolateBarsInPack;
		private int candiesInPack;

		/// <summary>
		/// Создать конфету
		/// </summary>
		/// <returns>Candy</returns>
		public abstract Candy CreateCandy();

		/// <summary>
		/// Создать шоколадку
		/// </summary>
		/// <returns>ChocolateBar</returns>
		public abstract ChocolateBar CreateChocolateBar();

		/// <summary>
		/// Геттеры для параметров
		/// </summary>

		public string Name() {
			return this.name;
		}

		public int ChocolateBarsInPack() {
			return this.chocolateBarsInPack;
		}

		public int CandiesInPack() {
			return this.candiesInPack;
		}
	}
}