using System;
using System.Collections.Generic;

namespace DOTNET1 {
	public interface IFactory {
		/// <summary>
		/// Создать конфету
		/// </summary>
		/// <returns>Candy</returns>
		Candy CreateCandy();

		/// <summary>
		/// Создать шоколадку
		/// </summary>
		/// <returns>ChocolateBar</returns>
		ChocolateBar CreateChocolateBar();

		/// <summary>
		/// Геттеры для параметров
		/// </summary>
		string Name();
		int ChocolateBarsInPack();
		int CandiesInPack();
	}
}
