using System;
using System.Collections.Generic;

namespace DOTNET1 {
	class BlackChocolateFactory : AbstractFactory {
		public BlackChocolateFactory() : base(5, 3, "Black production") { }

		public override Candy CreateCandy() {
			var ferreroRocherCandy = new FerreroRocherCandy();
			return ferreroRocherCandy;
		}

		public override ChocolateBar CreateChocolateBar() {
			return new Lindt();
		}
	}
}
