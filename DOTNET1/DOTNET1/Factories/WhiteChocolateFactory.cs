using System;
using System.Collections.Generic;

namespace DOTNET1 {
	class WhiteChocolateFactory : AbstractFactory {
		public WhiteChocolateFactory() : base(10, 1, "White production") { }

		public override Candy CreateCandy() {
			return new RafaelloCandy();
		}

		public override ChocolateBar CreateChocolateBar() {
			return new Milka();
		}
	}
}
