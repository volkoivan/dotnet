﻿using System;
namespace DOTNET1 {
	public enum EventType {
		Add,
		Clear,
		Remove,
		Sort
	}

	public class GiftEventArgs {
		public GiftEventArgs(EventType actionType) {
			ActionType = actionType;
		}
		public EventType ActionType { get; }
	}

	public class ProductAddedEventArgs : GiftEventArgs {
		public ProductAddedEventArgs(Product product) : base(EventType.Add) {
			Product = product;
		}
		public Product Product { get; }
	}

	public class ProductRemovedEventArgs : GiftEventArgs {
		public ProductRemovedEventArgs(Product product) : base(EventType.Remove) {
			Product = product;
		}
		public Product Product { get; }
	}
}

