﻿using System;
namespace DOTNET1 {
	public class ExceptionLogger : Logger, IExceptionLogger {
		public ExceptionLogger(string path = null) : base(path) { }

		public void LogSystemException(Exception ex) {
			using (var writer = GetWriter()) {
				writer.WriteLine("{0}. Системное исключение ({1}) с сообщением \"{2}\"\n", DateTime.Now.ToString("HH:mm:ss.fff"), ex.GetType(), ex.Message);
			}
		}

		public void LogUserException(Exception ex) {
			using (var writer = GetWriter()) {
				writer.WriteLine("{0}. Пользователькое исключение ({1}) с сообщением \"{2}\"\n", DateTime.Now.ToString("HH:mm:ss.fff"), ex.GetType(), ex.Message);
			}
		}
	}
}
