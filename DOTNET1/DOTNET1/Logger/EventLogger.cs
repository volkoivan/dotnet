﻿using System;
using System.IO;
using System.Threading;

namespace DOTNET1 {
	public class EventLogger<T> : Logger, IEventLogger<T> where T : Gift {

		/// <summary>
		/// Логгируемый объект - подарок
		/// </summary>
		private readonly T gift;

		/// <summary>
		/// Событие для записи лога
		/// </summary>
		public event Action<TextWriter, T, GiftEventArgs> OnLog;

		/// <summary>
		/// Создает новый экземпляр класса логгер
		/// </summary>
		/// <param name="gift">Подарок.</param>
		/// <param name="filePath">Путь к файлу для вывода.</param>
		public EventLogger(T gift, string filePath = null) : base(filePath) {
			this.gift = gift;

			//подписываемся на события подарка
			gift.OnAdd += Gift_EventHandler;
			gift.OnSort += Gift_EventHandler;
			gift.OnClear += Gift_EventHandler;
			gift.OnRemove += Gift_EventHandler;
		}

        private readonly object locker = new object();
		/// <summary>
		/// Обработчик событий для логгера подарка
		/// </summary>
		/// <param name="args">Arguments.</param>
		private void Gift_EventHandler(GiftEventArgs args) {
				var ts = new ThreadStart(() => {
				lock (locker) {
					using (var writer = GetWriter()) {
						OnLog?.Invoke(writer, gift, args);
					}
				}
			});

			var thread = new Thread(ts) { IsBackground = true };
			thread.Start();
		}
	}
}
