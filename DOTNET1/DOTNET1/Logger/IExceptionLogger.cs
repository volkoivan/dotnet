﻿using System;
using System.IO;
namespace DOTNET1 {
	public interface IExceptionLogger {
		void LogSystemException(Exception ex);
		void LogUserException(Exception ex);
	}
}
