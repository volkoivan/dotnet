﻿using System;
using System.IO;

namespace DOTNET1 {
	public enum LoggingType {
		Console, File
	}

	public abstract class Logger : ILogger {
		public string FilePath { get; }
		public LoggingType LoggingType { get; }

		protected Logger(string filePath) {
			FilePath = filePath;
			if (filePath == null) {
				LoggingType = LoggingType.Console;
			} else {
				LoggingType = LoggingType.File;

				if (!File.Exists(filePath)) {
					var fs = File.Create(filePath); //закрываем файл после создания
					fs.Close();
				}
			}
		}

		protected TextWriter GetWriter() {
			return LoggingType == LoggingType.Console ? Console.Out : File.AppendText(FilePath);
		}
	}
} 
