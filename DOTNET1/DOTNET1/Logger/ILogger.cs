﻿using System;
using System.IO;

namespace DOTNET1 {
	public interface ILogger {
        LoggingType LoggingType { get; }
        string FilePath { get; }
	}
}