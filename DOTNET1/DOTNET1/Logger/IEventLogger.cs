﻿using System;
using System.IO;

namespace DOTNET1 {
	public interface IEventLogger<out T> where T : IGift {
		event Action<TextWriter, T, GiftEventArgs> OnLog;
	}
}
