﻿using System;
namespace DOTNET1 {
	public interface ISerializer<T> where T : Gift {
		/// <summary>
		/// Десериализация из файла
		/// </summary>
		/// <param name="path">путь к файлу</param>
		Gift Unserialize(string path);

		/// <summary>
		/// Сериализация подарка в файл
		/// </summary>
		/// <param name="gift">подарок</param>
		/// <param name="path">путь к файлу</param>
		void Serialize(Gift gift, string path);
	}
}
