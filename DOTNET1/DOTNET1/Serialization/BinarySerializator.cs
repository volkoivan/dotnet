﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DOTNET1
{
    /// <summary>
    /// Бинарный сериализатор
    /// </summary>
	public class BinarySerializator<T> : ISerializer<T> where T : Gift
    {
        private readonly BinaryFormatter formatter;
        public BinarySerializator()
        {
            formatter = new BinaryFormatter();
        }

        public Gift Unserialize(string path)
        {
            using (var fs = new FileStream(path, FileMode.Open))
            {
				var wrapper = (GiftWrapper) formatter.Deserialize(fs);
				return new Gift(wrapper.Products);
            }
        }

        public void Serialize(Gift gift, string path)
        {
            var wrapper = new GiftWrapper(gift);
            using (var fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, wrapper);
            }
        }
    }
}