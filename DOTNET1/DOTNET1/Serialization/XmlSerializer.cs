﻿using System.IO;
using System.Xml;

namespace DOTNET1
{
    /// <summary>
    /// XML сериализатор
    /// </summary>
	public class CustomXmlSerializer<T> : ISerializer<T> where T : Gift
    {
        public Gift Unserialize(string path)
        {
			var serializer = new XmlSerializer<GiftWrapper>();
			var wrapper = (GiftWrapper) serializer.Deserialize(path);
			return new Gift(wrapper.Products);
        }

        public void Serialize(Gift gift, string path)
        {
			var wrapper = new GiftWrapper(gift);

			var serializer = new XmlSerializer<GiftWrapper>();
			System.IO.TextWriter writeFile = new StreamWriter(path);
			serializer.Serialize(writeFile, wrapper);
        }
    }
}
