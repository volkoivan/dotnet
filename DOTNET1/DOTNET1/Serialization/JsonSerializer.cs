﻿using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DOTNET1
{
    /// <summary>
    /// Json сериализатор
    /// <remarks>
    /// Использует пакет Newtonsoft.Json
    /// </remarks>
    /// </summary>
    public class JsonSerializer<T> : ISerializer<T> where T : Gift
    {
		public Gift Unserialize(string path)
        {
            var json = File.ReadAllText(path);
			var wrapper = JsonConvert.DeserializeObject<GiftWrapper>(json);

			return new Gift(wrapper.Products);
        }

        public void Serialize(Gift gift, string path)
        {
			var wrapper = new GiftWrapper(gift);
			var json = JsonConvert.SerializeObject(wrapper, Formatting.Indented);
            File.WriteAllText(path, json);
        }
    }
}