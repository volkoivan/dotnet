﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace DOTNET1
{
    /// <summary>
    /// Класс-обертка для сереализации
    /// </summary>
    [Serializable]
	[System.Xml.Serialization.XmlIncludeAttribute(typeof(FerreroRocherCandy))]
	[System.Xml.Serialization.XmlIncludeAttribute(typeof(Lindt))]
    public class GiftWrapper
    {
		public List<Product> Products { get; set; }

        public GiftWrapper(Gift gift)
        {
			Products = gift.products;
        }

        public GiftWrapper()
        {
        }
    }
}
