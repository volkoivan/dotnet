﻿using System;

namespace DOTNET1 {
	public class Pair<C, B> : IPair<C, B> where C : ICandy where B : IChocolateBar {
		public B ChocolateBar { get; }
		public C Candy { get; }
		/// <summary>
		/// Создает экземпляр пары конфет
		/// </summary>
		/// <param name="candy">Конфет.</param>
		/// <param name="chocolateBar">Шоколадка.</param>
		public Pair(C candy, B chocolateBar) {
			Candy = candy;
			ChocolateBar = chocolateBar;
		}
	}
}

