using System;
using System.Collections;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;

namespace DOTNET1 {
	public delegate void GiftSorter(Gift gift, int left, int right);
	[Serializable]
	public class Gift : IGift, ICollection<Product>, IEnumerator {

		public event Action<GiftEventArgs> OnSort;
		public event Action<GiftEventArgs> OnAdd;
		public event Action<GiftEventArgs> OnRemove;
		public event Action<GiftEventArgs> OnClear;

		[System.Xml.Serialization.XmlArray]
		public List<Product> products = new List<Product>();
		int position = -1;

		/// <summary>
		/// Создает экземпляр подарка с определенным набором конфет и шоколадок
		/// </summary>
		/// <param name="candies">Конфеты.</param>
		/// <param name="chocolateBars">Шоколадка.</param>
		public Gift(List<Product> candies, List<Product> chocolateBars) {
			foreach (Product candy in candies) {
				products.Add(candy);
			}
			foreach (Product chocolateBar in chocolateBars) {
				products.Add(chocolateBar);
			}
		}

		public Gift(int candiesCount, int barsCount, string type) {
			if ((type != "black" && type != "white") || (candiesCount <= 0 && barsCount <= 0)) {
				throw new GiftCreateException($"Невозможно создать подарок с такими параметрами");
			}
			for (int i = 0; i < candiesCount; i++) {
				if (type == "black") {
					products.Add(new FerreroRocherCandy());
				} else if (type == "white") {
					products.Add(new RafaelloCandy());
				}
			}
			for (int i = 0; i < barsCount; i++) {
				if (type == "black") {
					products.Add(new Lindt());
				} else if (type == "white") {
					products.Add(new Milka());
				}
			}
		}

		public Gift(List<Product> products) {
			this.products = products;
		}

		/// <summary>
		/// Реализация методов для интерфейса ICollection<Product>
		/// </summary>
		public int Count {
			get {
				return products.Count;
			}
		}

		public bool IsReadOnly {
			get {
				return false;
			}
		}

		public void Clear() {
			OnClear?.Invoke(new GiftEventArgs(EventType.Clear));
			products.Clear();
		}

		public void Add(Product item) {
			OnAdd?.Invoke(new ProductAddedEventArgs(item));
			products.Add(item);
		}

		public void CopyTo(Product[] array, int arrayIndex) {
			if (array == null)
				throw new ArgumentNullException("Array is null");
			((ICollection<Product>)this).CopyTo(array, arrayIndex);
		}

		public bool Remove(Product item) {
			if (products.Count == 0) {
				throw new RemoveException("В массиве нечего удалять");
			} else if (!products.Contains(item)) {
				throw new RemoveException("Такого элемента нет в данном подарке");
			}
			OnRemove?.Invoke(new ProductRemovedEventArgs(item));
			return products.Remove(item);
		}

		public IEnumerator<Product> GetEnumerator() {
			return products.GetEnumerator();
		}

		public bool Contains(Product item) {
			return products.Contains(item);
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return products.GetEnumerator();
		}

		public List<Product> GetProducts() {
			return products;
		}

		/// <summary>
		/// Реализация методов для интерфейса IEnumerator
		/// </summary>
		public object Current {
			get {
				return products[position];
			}
		}

		public bool MoveNext() {
			if (position < products.Count - 1) {
				position++;
				return true;
			} else {
				return false;
			}
		}

		public void Reset() {
			position = -1;
		}

		/// <summary>
		/// Пример использования yield для подключения foreach
		/// </summary>
		/// <returns>Продукты подарка</returns>
		public IEnumerable<Product> GetElements() {
			for (int i = 0; i < products.Count; i++) {
				yield return products[i];
			}
		}


		/// <summary>
		/// Сортировка подарка
		/// </summary>
		/// <param name="sorter">Sorter.</param>
		public void Sort(Comparison<IProduct> sorter) {
			OnSort?.Invoke(new GiftEventArgs(EventType.Sort));
			products.Sort((a, b) => sorter(a, b));
		}

         /// <summary>
         /// Асинхронная сортировка подарка
         /// </summary>
         /// <param name="sorter">Sorter</param>
         /// <param name="progress">прогресс сортировки</param>
         public async Task SortAsync(Comparison<IProduct> sorter, IProgress<int> progress = null) {
			await Task.Run(() => {
				//Сортировка выбором
				int previousProgress = -1;
				for (int i = 0; i < products.Count; i++) {
					for (int j = 0; j < products.Count - 1; j++) {
						if (sorter(products[j], products[j + 1]) > 0) {
							var temp = products[j + 1];
							products[j + 1] = products[j];
							products[j] = temp;
						}
					}
					var prog = i * 100 / products.Count;
					if (prog != previousProgress) {
						progress?.Report(prog);
						previousProgress = prog;
					}
				}
				progress?.Report(100);
			});
		}

		/// <summary>
		/// Выводит информацию о каждом продукте в подарке в заданном делегатом формате
		/// </summary>
		public void PrintInfo(Func<IProduct, string> command) {
			foreach (Product product in products) {
				Console.WriteLine(command(product));
			}
		}

        public void PrintGift() {
			Console.WriteLine($"В состав подарка входят:");
			foreach (var product in products) {
				Console.WriteLine($"{product.name} весом {product.weight}г.");
			}
		}

		/// <summary>
		/// Производит чтение подарка
		/// </summary>
		/// <typeparam name="T">Тип продукции</typeparam>
		/// <param name="path">Путь к файлу</param>
		/// <exception cref="T:System.IO.FileNotFoundException">Файл, заданный в <paramref name="path"/>, не найден. </exception>
		/// <exception cref="T:DOTNET1.FileFormatException">Файл, заданный в <paramref name="path"/>, имеет неверный формат. </exception>
		public static Gift FromFile(string path) {
			var info = File.ReadAllLines(path);

			int candyCount, barsCount;
			string type;

			try {
				candyCount = int.Parse(info[0]);
				barsCount = int.Parse(info[1]);
				type = info[2];
			} catch (FormatException) { throw new FileFormatException("Входной файл неверного формата"); }
			Gift gift = new Gift(candyCount, barsCount, type);
			return gift;
		}

	}
}
