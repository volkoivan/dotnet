﻿using System;
using System.Threading.Tasks;

namespace DOTNET1 {
	public interface IGift {
		void Sort(Comparison<IProduct> sorter);
        Task SortAsync(Comparison<IProduct> sorter, IProgress<int> progress = null);
		void PrintInfo(Func<IProduct, string> command);
	}
}

