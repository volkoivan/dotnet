﻿using System;
namespace DOTNET1 {
	public interface IPair<out C, out B> where C: ICandy where B: IChocolateBar {
		/// <summary>
		/// Возвращает конфету из пары
		/// </summary>
		/// <value>Конфета.</value>
		C Candy { get; }
		/// <summary>
		/// Возвращает шоколадку из пары
		/// </summary>
		/// <value>Шоколадка.</value>
		B ChocolateBar { get; }
	}
}

