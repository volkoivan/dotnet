﻿using System;
namespace DOTNET1 {
	public class RemoveException: UserException {
		public RemoveException(string message) : base(message) { }
	}
}
