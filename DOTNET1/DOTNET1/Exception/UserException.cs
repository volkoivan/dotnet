﻿using System;
namespace DOTNET1 {
	public class UserException: Exception {
		protected UserException(string message) : base(message){ }
	}
}
