﻿using System;
namespace DOTNET1 {
	public class FileFormatException: UserException {
		public FileFormatException(string message) : base(message) {}
	}
}
