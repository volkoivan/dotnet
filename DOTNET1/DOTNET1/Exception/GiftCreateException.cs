﻿using System;
namespace DOTNET1 {
	public class GiftCreateException: UserException {
		public GiftCreateException(string message) : base(message) { }
	}
}
