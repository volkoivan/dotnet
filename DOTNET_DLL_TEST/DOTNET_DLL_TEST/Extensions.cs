﻿using System.Collections.Generic;
using DOTNET1;
using System.Linq;
using System.IO;
using Newtonsoft.Json;

namespace DOTNET_DLL_TEST
{
	public static class Extensions
	{
		/// <summary>
		/// Finds the products with weight more than.
		/// </summary>
		/// <returns>The products with weight more than.</returns>
		/// <param name="weight">Weight.</param>
		/// <param name="gift">Gift.</param>
		/// <param name="logger">Logger.</param>
		public static List<DOTNET1.Product> FindProductsWithWeightMoreThan<T>(this Gift gift, int weight) where T : Product
		{
			System.Console.WriteLine("Продукты отфильтрованы по весу методом из расширения");
			gift.products = gift.products.Where(g => g.weight > weight).ToList();
			return gift.products.Where(g => g.weight > weight).ToList();
		}

		/// <summary>
		/// Finds the products with name longer than.
		/// </summary>
		/// <returns>The products with name longer than.</returns>
		/// <param name="length">Length.</param>
		/// <param name="gift">Gift.</param>
		/// <param name="logger">Logger.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static List<DOTNET1.Product> FindProductsWithNameLongerThan<T>(this Gift gift, int length) where T : Product
		{
			System.Console.WriteLine("Продукты отфильтрованы по имени методом из расширения");
			return gift.products.Where(g => g.name.Length > length).ToList();
		}

		/// <summary>
		/// Converts to string.
		/// </summary>
		/// <returns>The to string.</returns>
		/// <param name="bat">Bat.</param>
		/// <param name="logger">Logger.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static string ConvertToString<T>(this Gift gift) where T : Product
		{
			System.Console.WriteLine("Вызван метод преобразования коллекции в строку");
			var wrapper = new GiftWrapper(gift);
			return JsonConvert.SerializeObject(wrapper, Formatting.Indented);
		}
	}
}
