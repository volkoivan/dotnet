﻿using System;
using DOTNET1;

namespace DOTNET_DLL_TEST
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			var gift = new Gift(5, 4, "black");
			gift.PrintGift();
			Console.WriteLine(gift.products.Count);
			var products = gift.FindProductsWithNameLongerThan<Product>(6);
			Console.WriteLine(products.Count);
			var products2 = gift.FindProductsWithWeightMoreThan<Product>(50);
			Console.WriteLine(products2.Count);
			var convertedString = gift.ConvertToString<Product>();
			Console.WriteLine(convertedString);
		}
	}
}
